 CREATE TABLE `challenge`.`contact_form` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT ,
`full_name` VARCHAR( 25 ) NOT NULL ,
`email` VARCHAR( 25 ) NOT NULL ,
`phone` VARCHAR( 25 ) NULL ,
`message` TEXT NOT NULL ,
`submitted` DATETIME NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;