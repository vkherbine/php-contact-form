<?php
include "contact_form_validate.php";

$name = $_POST['name']; // required
$email_from = $_POST['email']; // required
$phone = $_POST['phone']; // not required
$message = $_POST['message']; // required
$errors_array = array();

// check if any required fields are missing data
$errors_array = validate($_POST);

if(empty($errors_array)) {   
    $email_to = "guy-smiley@example.com";
    $email_subject = "Contact Form submitted at Dealer Inspire";
 
    $email_message = "Form details below.\n\n";
      
    $bad = array("content-type","bcc:","to:","cc:","href");
  
    $email_message .= "Name: ".str_replace($bad, "", $name)."\n";
    $email_message .= "Email: ".str_replace($bad, "", $email_from)."\n";
    $email_message .= "Phone: ".str_replace($bad, "", $phone)."\n";
    $email_message .= "Message: ".str_replace($bad, "", $message)."\n";
 
    // create email headers
    $headers = 'From: '.$email_from."\r\n".
    'Reply-To: '.$email_from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
    @mail($email_to, $email_subject, $email_message, $headers);
    
    $response = "Thank you for contacting us. We will be in touch with you very soon.";
    
    $host = '127.0.0.1';
    $db   = 'challenge';
    $user = 'root';
    $pass = '';
    
    try{
        $db = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $db->prepare("INSERT INTO `challenge`.`contact_form` (full_name,email,phone,message,submitted) VALUES(:name,:email,:phone,:message,NOW())");
        $stmt->execute(array(':name' => $name, ':email' => $email_from, ':phone' => $phone, ':message' => $message));
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
        $response = "Your form could not be submitted.  Please try again later.";
    }
    
    ?>
 
    <script type="text/javascript">
        alert("<?php echo $response; ?>");
        window.location.href="index.html";
    </script>
 
<?php   
    
} else {
    $error_message = "";
    foreach($errors_array as $error){
        $error_message .= $error;
    }
    ?>
    <script type="text/javascript">
        alert("Your form could not be submitted.  Please correct the following errors:\n\n <?php echo $error_message; ?>");
        window.location.href="index.html#contact";
    </script>
<?php
}
?>