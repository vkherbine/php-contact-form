<?php
/*
 * validate required fields
 * 
 */
function validate($data){
    $errors = array();
    $string_exp = "/^[A-Za-z .'-]+$/";
    
    if(!isset($data['name'])){
        $errors['name'] = "Please provide your name.\\n";
    } else {
        if(!preg_match($string_exp,$data['name'])) {
            $errors['name'] = "The name you provided does not appear to be valid.\\n";
        }
    }

    if(!isset($data['email'])){
        $errors['email'] = "Please provide your email address.\\n";
    } else {
        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
     
        if(!preg_match($email_exp,$data['email'])) {
            $errors['email'] = "The email address you provided does not appear to be valid.\\n";
        }
    }
    
    if(!isset($data['message'])) {
        $errors['message'] = "Please provide a message\\n";     
    } else {
        if(!preg_match($string_exp,$data['message'])) {
            $errors['message'] = "The message you provided does not appear to be valid.\\n";
        }
    }
    return $errors;
}
?>