<?php

include 'contact_form_validate.php';

class ValidateTest extends PHPUnit_Framework_TestCase
{

    public function testValidate()
    {
        $input = array('name' => 'Mickey Mouse','email' => 'mickey@disney.com','message' => 'call me');
        $return = validate($input);
        $this->assertEmpty($return);
        
        $input = array('name' => 'Donald Duck','email' => 'donald','message' => 'hello');
        $return = validate($input);
        $this->assertEquals(array('email' => "The email address you provided does not appear to be valid.\\n"), $return);
    }
}
?>